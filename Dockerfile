FROM haproxy
LABEL maintainer="Krerk Piromsopa, Ph.D. <krerk.p@chula.ac.th>"
USER root
RUN apt-get update && apt-get install -y \
	mariadb-client

COPY autoconfig_entrypoint.sh /

EXPOSE 3306

ENTRYPOINT ["/autoconfig_entrypoint.sh"]
