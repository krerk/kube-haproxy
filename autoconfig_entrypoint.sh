#!/bin/bash

if [ -z $REPSET_DOMAINNAME ]
then
	REPSET_DOMAINNAME=default.svc.cluster.local
fi

if [ -z $REPSET_NAME ]
then
	REPSET_NAME=cluster
fi

if [ -z $REPSET_PORT ]
then
	REPSET_PORT=80
fi

if [ -z $PROXY_PORT ]
then
	PROXY_PORT=80
fi

if [ -z $PROXY_MODE ]
then
	PROXY_MODE=tcp
fi

if [ -z $PROXY_OPTION ]
then
	PROXY_OPTION="mysql-check user proxy_check"
fi

if [ -z $PROXY_BALANCE ]
then
	PROXY_BALANCE=leastconn
fi

if [ -z $NODE_COUNT ]
then
	NODE_COUNT=1
fi

echo "Deverhood MariaDB Cluster configuration"
echo "======================================="
echo "Generate haproxy.cfg"
echo "   proxy port  : $PROXY_PORT"
echo "   mode        : $PROXY_MODE"
echo "   option      : $PROXY_OPTION"
echo "   balance     : $PROXY_BALANCE"
echo "statefulSet"
echo "   statefulSet : $REPSET_NAME"
echo "   base domain : $REPSET_DOMAINNAME"
echo "   port        : $REPSET_PORT"
echo "   min node    : $NODE_COUNT"

_base_config () {
cat <<EOL >/usr/local/etc/haproxy.cfg
global
    log stdout format raw local0
    user proxy
    group proxy

defaults
    log global
    retries 2
    timeout connect 10s
    timeout server 10800s
    timeout client 10800s

listen http-stats
    bind 0.0.0.0:8080
    mode http
    stats enable
    stats uri /
    stats realm Status\/Private\ Zone
    stats auth admin:deverhood

listen mariadb-cluster
    bind 0.0.0.0:$PROXY_PORT
    mode $PROXY_MODE
    option  $PROXY_OPTION
    balance $PROXY_BALANCE

EOL
i=0
while [ $i -lt $NODECNT ]
do
cat << EOL >>/usr/local/etc/haproxy.cfg
    server $REPSET_NAME-$i $REPSET_NAME-$i.$REPSET_NAME.$REPSET_DOMAINNAME:$REPSET_PORT check
EOL
    i=$(( $i + 1))
done
echo =======================================
echo "haproxy.cfg for $NODECNT node(s)"
echo =======================================
cat /usr/local/etc/haproxy.cfg
echo =======================================
}

_node_count () {
	i=-1;	
	IS_OPEN='open'
	while [ ! -z "$IS_OPEN" ]
	do	
		NOW=$( date )
		i=$(( $i + 1 ))
		echo "$NOW > probe for $REPSET_NAME-$i.$REPSET_NAME.$REPSET_DOMAINNAME:$REPSET_PORT"
		#IS_OPEN=`nc -zvw3 $REPSET_NAME-$i.$REPSET_NAME.$REPSET_DOMAINNAME. $REPSET_PORT 2>&1| grep open`
		IS_OPEN=`mysqladmin -h $REPSET_NAME-$i.$REPSET_NAME.$REPSET_DOMAINNAME. -u proxy_check ping 2>&1 | grep alive`
                if [ ! -z "$IS_OPEN" ]
		then
			echo " ... alive"
		else
			echo " ... dead"
		fi
	done
	# CNT should be 1 (minimum)
	if [ $i -eq 0 ]
	then
		echo "no node found, fix at $NODE_COUNT node."
		i=$NODE_COUNT
	fi
	return $i
}

_start_haproxy () {
	haproxy -D -f /usr/local/etc/haproxy.cfg -p /haproxy.pid 
}

_restart_haproxy () {
	haproxy -D -f /usr/local/etc/haproxy.cfg -p /haproxy.pid -sf $(cat /haproxy.pid)
}

_node_count
NODECNT=$?
_base_config
_start_haproxy 

while [ true ];
do
	echo "Sleep 60s before checking for addtional node"
	sleep  60
	_node_count
	CNT=$?
	if [ $CNT -ne $NODECNT ]
	then
		NODECNT=$CNT
		_base_config
		_restart_haproxy
	fi
done
